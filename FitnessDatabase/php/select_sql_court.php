<?php
/*
 * Coded By: Dmitri Tulonen 
 * Coded For: CIS_2250 Mobile Application Development
 * Date: 22/01/2014
 */
 
/*
 * Following code will select everything from a table in the database and return it with JSON
 * Using The Code Requires
 * http://localhost/select_sql.php?sql=tableName&fields=field1~field2~field3
 */

// array for JSON response
$response = array();
$fields = null;

// include db connect class
require_once __DIR__ . '/db_connect.php';

// connecting to db
$db = new DB_CONNECT();
// grab all the data from the table provided in the url
	
$query1 = "SELECT court_times.start_time, court.court_name, court.court_number FROM court_times , court  WHERE court_times.start_time > ";
$query2 = "'" .$_GET['starttime']. "' " ;
$query3 = " and NOT EXISTS (select * from court_bookings where  court_bookings.booking_date = ";  //(0 = select count(*) from court_bookings where
$query3a = "'" .$_GET['bookingdate']. "' " ;
$query3b = " and court.court_number = court_bookings.court_number and court_times.start_time = court_bookings.start_time) ";
$query4 = " ORDER BY court_times.start_time, court.court_name";
$query = $query1.$query2.$query3.$query3a.$query3b.$query4;

//print_r($query);
// make a list of the fields the person wishes to retrive currently using tilde "~" as a deliminator
$result = mysql_query($query) or die(mysql_error());

    $fieldList = "start_time~court_name~court_number~member_id";
	$fields = explode("~", $fieldList);
	//print_r($fields);


// check for empty result
if (mysql_num_rows($result) > 0) {
    // looping through all results
    
    $response["CisFitness"] = array();
    
    while ($row = mysql_fetch_array($result)) {
        // temp user array
        $CisFitness = array();
		// loop to get all columns requested
		foreach ($fields as &$value) {
			$CisFitness["$value"] = $row["$value"];
		}
        // push single object into final response array
        array_push($response["CisFitness"], $CisFitness);
    }
    // success
    $response["success"] = 1;

    // echoing JSON response
    echo json_encode($response);
} else {
    // no results found
    $response["success"] = 0;
    $response["message"] = "No results found";

    // echo no users JSON
    echo json_encode($response);
}
?>
